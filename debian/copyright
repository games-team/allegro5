Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Allegro 5
Upstream-Contact: allegro-developers@lists.liballeg.org
Files-Excluded: demos/*
                examples/data/haiku/*
                examples/data/DejaVuSans.*
                addons/audio/kcm_mixer_helpers.inc
                include/allegro5/opengl/GLext/*gl*_ext_alias.h
                src/convert.c include/allegro5/internal/aintern_convert.h
                src/scanline_drawers.*
                addons/primitives/directx_shaders.cpp
                android/gradle_project/gradle/wrapper/gradle-wrapper.jar
Source: http://liballeg.org/

Files: *
Copyright: Copyright (c) 2004-2016 the Allegro 5 Development Team
License: Zlib

Files: debian/*
Copyright: 2012-2016 Tobias Hansen <thansen@debian.org>
           2012-2024 Andreas Rönnquist <gusnan@debian.org>
License: Zlib

Files: docs/scripts/trex.*
Copyright: Copyright (C) 2003-2006 Alberto Demichelis
License: Zlib

Files: docs/src/autosuggest.js
Copyright: (c) 2007-2009 Dmitriy Khudorozhkov (dmitrykhudorozhkov@yahoo.com)
License: Zlib

Files: examples/ex_curl.c
Copyright: Coyright (c)2003 Simtec Electronics
License: BSD-3-clause

Files: src/misc/bstrlib.*
Copyright: Copyright (c) 2002-2008 Paul Hsieh
License: BSD-3-clause or GPL-2

Files: src/win/wclipboard.c
       src/x/xclipboard.c
Copyright: Copyright (C) 1997-2013 Sam Lantinga <slouken@libsdl.org>
License: Zlib

License: Zlib
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
 claim that you wrote the original software. If you use this software
 in a product, an acknowledgment in the product documentation would be
 appreciated but is not required.
 .
 2. Altered source versions must be plainly marked as such, and must not be
 misrepresented as being the original software.
 .
 3. This notice may not be removed or altered from any source
 distribution.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 .
 Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 Neither the name of the software nor the names of its contributors may be used
 to endorse or promote products derived from this software without
 specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 2 as
 published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in /usr/share/common-licenses/GPL-2
